*** Keywords ***

LAUNCH 5GMARK APPLICATION
    [Arguments]  ${test_name}=Test  ${device_name}=None  
    Open Application  ${APPIUM_SERVER}  platformName=${PLATFORM_NAME}  platformVersion=${PLATFORM_VERSION}  appActivity=${APP_ACTIVITY}  appPackage=${APP_PACKAGE}  udid=${TEST_DEVICE}  usePrebuiltWDA=true  automationName=UiAutomator2  newCommandTimeout=60000  useNewWDA=false  autoGrantPermissions=true  autoAcceptAlerts=true

DISPLAY RESULT TO CONSOLE
    wait until page contains element  ${RESULT_PAGE}  timeout=240
    click element  ${EXPLANATIONS_TAB}
    builtin.sleep  5
    ${get_downlink_result}  get text  ${EXPLANATIONS_DOWNLOAD}
    log to console  ${get_downlink_result} 
    ${get_uploadlink_result}  get text  ${EXPLANATIONS_UPLOAD}
    log to console  ${get_uploadlink_result}    

DISPLAY FULL RESULT TO CONSOLE    
    DISPLAY RESULT TO CONSOLE
    ${get_stream_result}  get text  ${EXPLANATIONS_STREAM}
    log to console  ${get_stream_result} 
    ${get_web_result}  get text  ${EXPLANATIONS_WEB}
    log to console  ${get_web_result}