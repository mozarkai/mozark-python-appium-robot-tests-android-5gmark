*** Settings ***
Library         AppiumLibrary
Resource        ${EXECDIR}/Resources/resource.robot
Variables       ${EXECDIR}/VariableFiles/config.py

*** Test Cases ***

TC-001 Change Mbps To mBps And Verify The Same In History Tab
    [Tags]  MbpsTomBps
    LAUNCH 5GMARK APPLICATION
    wait until page contains element  ${ACCOUNT_SETTINGS_TAB}  timeout=10
    click element  ${ACCOUNT_SETTINGS_TAB}
    wait until page contains element  ${DISPLAYED_DATA_UNIT}  timeout=10
    click element  ${DISPLAYED_DATA_UNIT}
    wait until page contains element  ${last_UNIT}  timeout=5
    click element  ${last_UNIT}
    wait until page contains element  ${TEST_TAB}  timeout=5
    click element  ${TEST_TAB}
    builtin.sleep  3
    click element  ${SPEED_TEST}
    click element  ${TEST_BUTTON}
    wait until page contains element  ${INDOOR_TEST}  timeout=5
    click element  ${INDOOR_TEST}
    DISPLAY RESULT TO CONSOLE 
    press keycode    4
    wait until page contains element  ${HISTORY_TAB}  timeout=5
    click element  ${HISTORY_TAB}
    wait until page contains element  ${FIRST_RECORD_HISTORY_COLUMN}  timeout=10
    ${get_text}  get text  ${FIRST_DOWNLINK_RECORD} 
    should match regexp  ${get_text}  [^0-9]mBps
    ${get_text}  get text  ${FIRST_UPLINK_RECORD} 
    should match regexp  ${get_text}  [^0-9]mBps
    log to console  Reverting data unit settings
    click element  ${ACCOUNT_SETTINGS_TAB}
    wait until page contains element  ${DISPLAYED_DATA_UNIT}  timeout=10
    click element  ${DISPLAYED_DATA_UNIT}
    wait until page contains element  ${kBps_UNIT}  timeout=5
    click element  ${Mbps_UNIT}
    quit application

TC-002: Outdoor Speed Test
    [Tags]  OutdoorSpeedTest
    LAUNCH 5GMARK APPLICATION
    wait until page contains element  ${SPEED_TEST}  timeout=10
    click element  ${SPEED_TEST}
    click element  ${TEST_BUTTON}
    wait until page contains element  ${OUTDOOR_TEST}  timeout=5
    click element  ${OUTDOOR_TEST}
    DISPLAY RESULT TO CONSOLE
    quit application    