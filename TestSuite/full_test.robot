*** Settings ***
Library         AppiumLibrary
Resource        ${EXECDIR}/Resources/resource.robot
Variables       ${EXECDIR}/VariableFiles/config.py

*** Test Cases ***

TC-001: Indoor Full Test
    [Tags]  IndoorFullTest
    LAUNCH 5GMARK APPLICATION
    wait until page contains element  ${FULL_TEST}  timeout=10
    click element  ${FULL_TEST}
    click element  ${TEST_BUTTON}
    wait until page contains element  ${INDOOR_TEST}  timeout=5
    click element  ${INDOOR_TEST}
    DISPLAY FULL RESULT TO CONSOLE
    press keycode    4
    wait until page contains element  ${HISTORY_TAB}  timeout=10
    click element  ${HISTORY_TAB}   
    wait until page contains element  ${FIRST_RECORD_HISTORY_COLUMN}  timeout=10
    click element  ${FIRST_RECORD_HISTORY_COLUMN}
    ${RESULT_PAGE}  run keyword and return status  wait until page contains element  ${RESULT_PAGE}  timeout=10
    quit application           