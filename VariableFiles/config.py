import os

# Device Configuration
TEST_DEVICE = os.environ['DEVICE_SERIAL_ID']
PLATFORM_NAME = 'Android'
PLATFORM_VERSION = os.environ['DEVICE_OS_VERSION']
WDALOCALPORT = '8100'

# APPIUM_URL = 'http://127.0.0.1:4723/wd/hub'
APPIUM_SERVER = os.environ['APPIUM_SERVER']

# Application Details
AUTOMATION_NAME = 'UiAutomator2'
APP_PACKAGE='com.agence3pp'
APP_ACTIVITY='qosi.fr.usingqosiframework.splashscreen.SplashscreenActivity'

# Alerts After Launching App
ALERT = 'com.agence3pp:id/alertTitle'
ALERT_OK = 'android:id/button1'

#Different Tabs
PODIUM_MAP_TAB = 'com.agence3pp:id/navigation_1'
HISTORY_TAB = 'com.agence3pp:id/navigation_2'
TEST_TAB = 'com.agence3pp:id/navigation_3'
MONITORING_TAB = 'com.agence3pp:id/navigation_4'
ACCOUNT_SETTINGS_TAB = 'com.agence3pp:id/navigation_5'

# History Tab
FIRST_RECORD_HISTORY_COLUMN = '//android.widget.FrameLayout[1]//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]'
FIRST_DOWNLINK_RECORD = '//android.widget.FrameLayout[1]//android.view.ViewGroup[1]/android.widget.TextView[3]'
FIRST_UPLINK_RECORD = '//android.widget.FrameLayout[1]//android.view.ViewGroup[1]/android.widget.TextView[4]'

# Test Types
FULL_TEST = 'com.agence3pp:id/id_test_scenario_full'
SPEED_TEST = 'com.agence3pp:id/id_test_scenario_speed'
DRIVE_TEST = 'com.agence3pp:id/id_test_scenario_drive'
INDOOR_TEST = 'com.agence3pp:id/test_bottom_sheet_1'
OUTDOOR_TEST = 'com.agence3pp:id/test_bottom_sheet_2'
CAR_TEST = 'com.agence3pp:id/test_bottom_sheet_3'
TRAIN_TEST = 'com.agence3pp:id/test_bottom_sheet_4'
TEST_BUTTON = '//android.widget.ImageView[@content-desc="Here\'s an image or an icon"]'

RESULT_PAGE = '//androidx.appcompat.app.ActionBar.Tab[@content-desc="Results"]/android.widget.TextView'
DOWNLINK_RESULT = 'com.agence3pp:id/id_value_1_progress_bar'
UPLINK_RESULT = 'com.agence3pp:id/id_value_2_progress_bar'
DRIVE_TEST_RESULT = 'com.agence3pp:id/id_test_result_value_1'

EXPLANATIONS_TAB = '//androidx.appcompat.app.ActionBar.Tab[@content-desc="Explanations"]'
EXPLANATIONS_DOWNLOAD = '//android.view.ViewGroup[1]/android.widget.TextView[2]'
EXPLANATIONS_UPLOAD = '//android.view.ViewGroup[2]/android.widget.TextView[2]'
EXPLANATIONS_STREAM = '//android.view.ViewGroup[3]/android.widget.TextView[2]'
EXPLANATIONS_WEB = '//android.view.ViewGroup[4]/android.widget.TextView[2]'

# Account Settings Tab
DISPLAYED_DATA_UNIT = '//android.widget.FrameLayout[1]//android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.TextView[1]'
kbps_UNIT = '//android.widget.CheckedTextView[1]'
Mbps_UNIT = '//android.widget.CheckedTextView[2]'
kBps_UNIT = '//android.widget.CheckedTextView[3]'
last_UNIT = '//android.widget.CheckedTextView[4]'
CANCEL_OPTION = 'android:id/button2'